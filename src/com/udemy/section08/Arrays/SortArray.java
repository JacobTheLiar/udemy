/*
Ch 53 - arrays
Create a program using arrays that sorts a list of integers in descending order.
Descending order is highest value to lowest.
In other words if the array had the values in it 106, 26, 81, 5, 15 your program should
ultimately have an array with 106,81,26, 15, 5 in it.
Set up the program so that the numbers to sort are read in from the keyboard.
Implement the following methods - getIntegers, printArray, and sortIntegers
getIntegers returns an array of entered integers from keyboard
printArray prints out the contents of the array
and sortIntegers should sort the array and return a new array containing the sorted numbers
you will have to figure out how to copy the array elements from the passed array into a new
array and sort them and return the new sorted array.
 */
package com.udemy.section08.Arrays;

import java.util.Scanner;

public class SortArray {
   
    private final Scanner scanner = new Scanner(System.in);
    
    public void doProgram(){
        
        int[] myArray = getIntegers(5);
        
        System.out.println("Inserted numbers:");
        printArray(myArray);

        System.out.println("Sorted numbers:");
        int[] sortedArray = copyArray(myArray);
        printArray(sortArray(sortedArray));
        
    }
    
    
    private int[] getIntegers(int count){
        System.out.println("Insert a few numbers:\r");
        int[] array = new int[count];
        for (int i=0; i<count; i++)
            array[i] = scanner.nextInt();
        
        return array;
    }

    private void printArray(int[] array){
        for (int i=0; i<array.length; i++)
            System.err.println("array["+i+"] = "+array[i]);
    }
    
    private int[] sortArray(int[] array){
        for (int i=0; i<array.length; i++){
            for (int j=i; j<array.length; j++){
                if (array[i]<array[j]){
                    int t = array[i];
                    array[i] = array[j];
                    array[j] = t;
                }
            }
        }
        return array;
    }
    
    private int[] copyArray(int[] array){
        int[] newArray = new int[array.length];
        for (int i=0; i<array.length; i++)
            newArray[i] = array[i];        
        return newArray;
    }
    
}
