/*
Lecture 60
 */
package com.udemy.section06;

/**
Create a new class VipCustomer
it should have 3 fields name, credit limit, and email address.
create 3 constructors
1st constructor empty should call the constructor with 3 parameters with default values
2nd constructor should pass on the 2 values it receives and add a default value for the 3rd
3rd constructor should save all fields.
create getters only for this using code generation of intellij as setters wont be needed
test and confirm it works.*/
public class VipCustomer {
    
    
    private String fName;
    private double fCreditLimit;
    private String fEMail;

    public VipCustomer(String aName, double aCreditLimit, String aEMail) {
        this.fName = aName;
        this.fCreditLimit = aCreditLimit;
        this.fEMail = aEMail;
    }

    public VipCustomer(String aName, double aCreditLimit) {
        this(aName, aCreditLimit, "no e-mail");
    }

    public VipCustomer() {
        this("no name", 0.00);
    }

    
    
    public String getName() {
        return fName;
    }

    public double getCreditLimit() {
        return fCreditLimit;
    }

    public String getEMail() {
        return fEMail;
    }
    
    
    
    
}
