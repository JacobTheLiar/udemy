/*
Ch 41 - Inheritance
 */
package com.udemy.section06;

public class Car extends Vechicle{
    
    private int wheels;
    private int doors;
    private int gears;
    private boolean isManual;
    private int currentGear;

    public Car(int wheels, int doors, int gears, boolean isManual, String name, String model) {
        super(name, model);
        this.wheels = wheels;
        this.doors = doors;
        this.gears = gears;
        this.isManual = isManual;
        this.currentGear = 1;
    }

    public void changeGear(int currentGear) {
        this.currentGear = currentGear;
    }
    
    public void changeSpeed(int speed, int direction){
        move(speed, direction);
    }
            

    
    
}
