/*
    lecture 59
*/
package com.udemy.section06;

/**
Create a new class for a bank account
Create fields for the account number, balance, customer name, email and phone number.

Create getters and setters for each field
Create two additional methods
1. To allow the customer to deposit funds (this should increment the balance field).
2. To allow the customer to withdraw funds. This should deduct from the balance field,
but not allow the withdrawal to complete if their are insufficient funds.
You will want to create various code in the Main class (the one created by IntelliJ) to
confirm your code is working.
Add some System.out.println's in the two methods above as well.
 */
public class BankAccountChallenge {

    private String fAccountNumber;
    private double fBalance;
    private String fCustomerName;
    private String fEMail;
    private String fPhoneNumber;

    
    public BankAccountChallenge(){
        this("00000", 0.00, "Unnamed", "No address", "No phone");
        System.out.println("Empty constructor fired");
    }

    public BankAccountChallenge(String aAccountNumber, double aBalance, String aCustomerName,
            String aEMail, String aPhoneNumber){
        System.out.println("Filled constructor fired");
        fAccountNumber = aAccountNumber;
        fBalance = aBalance;
        fCustomerName = aCustomerName;
        fEMail = aEMail;
        fPhoneNumber = aPhoneNumber;                
    }
    
    public void depositFunds(double aDepositAmount){
        fBalance += aDepositAmount;
        System.out.println("Deposit of "+aDepositAmount + " processed. New balance is "+fBalance);
    }

    public void withdrawFunds(double aWhithdrawAmount){
        boolean canWhithdraw = aWhithdrawAmount<=fBalance; 
        if (canWhithdraw) {
            fBalance -= aWhithdrawAmount;
            System.out.println("Withdrawal of "+aWhithdrawAmount + " processed. Remaining balance is "+fBalance);
        } else {
            System.out.println("Only "+fBalance+" available. Withdrawal not processed.");
        }
        
    }

    
    public String getAccountNumber() {
        return fAccountNumber;
    }

    public void setAccountNumber(String aAccountNumber) {
        fAccountNumber = aAccountNumber;
    }

    public double getBalance() {
        return fBalance;
    }

    public void setBalance(double aBalance) {
        fBalance = aBalance;
    }

    public String getCustomerName() {
        return fCustomerName;
    }

    public void setCustomerName(String aCustomerName) {
        fCustomerName = aCustomerName;
    }

    public String getEMail() {
        return fEMail;
    }

    public void setEMail(String aEMail) {
        fEMail = aEMail;
    }

    public String getPhoneNumber() {
        return fPhoneNumber;
    }

    public void setPhoneNumber(String aPhoneNumber) {
        fPhoneNumber = aPhoneNumber;
    }

    
}
