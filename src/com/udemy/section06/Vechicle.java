/*
Ch 41 - Inheritance
*/
package com.udemy.section06;

/**
Challenge.
Start with a base class of a Vehicle, then create a Car class that inherits from this base class.
Finally, create another class, a specific type of Car that inherits from the Car class.
You should be able to hand steering, changing gears, and moving (speed in other words).
You will want to decide where to put the appropriate state and behaviours (fields and methods).
As mentioned above, changing gears, increasing/decreasing speed should be included.
For you specific type of vehicle you will want to add something specific for that type of car.
 */
public class Vechicle {
    
    private String name;
    private String model;
    private int currentSpeed;
    private int currentDirection;
    
    public Vechicle(String name, String model) {
        this.name = name;
        this.model = model;
        
        this.currentDirection = 0;
        this.currentSpeed = 0;
    }
    
    public void steer(int direction){
        this.currentDirection += direction;
    }
    
    public void move (int speed, int direction){
        this.currentSpeed = speed;
        this.currentDirection = direction;
    }

    public void stop(){
       this.currentSpeed = 0;
    }    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(int currentSpeed) {
        this.currentSpeed = currentSpeed;
    }

    public int getCurrentDirection() {
        return currentDirection;
    }

    public void setCurrentDirection(int currentDirection) {
        this.currentDirection = currentDirection;
    }
 
    
    
}
