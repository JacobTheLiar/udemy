/*
Ch - Reverse Array
 */
package com.udemy.section09.ReverseArrayChallenge;


public class ReverseArray {


    public int[] reverseArray(int[] aArray){
        int[] reversedArray = new int[aArray.length];
        
        int reverseHelp = reversedArray.length;
        
        for(int i=0; i<reversedArray.length; i++){
            reverseHelp--;
            reversedArray[reverseHelp] = aArray[i];
        }
        
        return reversedArray;
    }
    
}
