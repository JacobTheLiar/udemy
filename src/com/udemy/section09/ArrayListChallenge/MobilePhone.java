package com.udemy.section09.ArrayListChallenge;

import java.util.ArrayList;

/*
Ch - Contact
 Create a program that implements a simple mobile phone with the following capabilities.
 Able to store, modify, remove and query contact names.
 You will want to create a separate class for Contact (name and phone number).
 Create a master class (MobilePhone) that holds the ArrayList of Contact
 The MobilePhone class has the functionality listed above.
 Add a menu of options that are available.
 Options:  Quit, print list of contacts, add new contact, update existing contact, remove contact
 and search/find contact.
 When adding or updating be sure to check if the contact already exists (use name)
 Be sure not to expose the inner workings of the Arraylist to Contact
 e.g. no ints, no .get(i) etc
 Contact should do everything with Contact objects only.
 */
public class MobilePhone {

    private final ArrayList<Contact> fContacts;

    public MobilePhone() {
        fContacts = new ArrayList<>();
    }

    private int findPosition(String aContactName) {

        for (int i = 0; i < fContacts.size(); i++) {
            if (fContacts.get(i).getContactName().equals(aContactName)) {
                return i;
            }
        }

        return -1;
    }

    public void addContact(String aName, String aNumber) {

        if (findPosition(aName) < 0) {
            Contact newConact = new Contact(aName, aNumber);
            fContacts.add(newConact);
        }
    }

    public void modifyContact(String aName, String aNewNumber) {
        int position = findPosition(aName);
        if (position >= 0) {
            fContacts.get(position).setContactNumber(aNewNumber);
        }
    }
    
    public void removeContact(String aName){
        int position = findPosition(aName);
        if (position >= 0) {
            fContacts.remove(position);
        }        
    }
    
    public String getContactPhone(String aName){
        int position = findPosition(aName);
        if (position >= 0) 
            return fContacts.get(position).getContactNumber();
        
        return "Not found!";
    }
    
    public void printNumbers(){
        for (int i = 0; i < fContacts.size(); i++) 
            fContacts.get(i).printContact();
    }

}
