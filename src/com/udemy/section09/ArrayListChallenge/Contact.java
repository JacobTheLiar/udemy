/*
Ch - Contact
 Create a program that implements a simple mobile phone with the following capabilities.
 Able to store, modify, remove and query contact names.
 You will want to create a separate class for Contact (name and phone number).
 Create a master class (MobilePhone) that holds the ArrayList of Contact
 The MobilePhone class has the functionality listed above.
 Add a menu of options that are available.
 Options:  Quit, print list of contacts, add new contact, update existing contact, remove contact
 and search/find contact.
 When adding or updating be sure to check if the contact already exists (use name)
 Be sure not to expose the inner workings of the Arraylist to Contact
 e.g. no ints, no .get(i) etc
 Contact should do everything with Contact objects only.
*/
package com.udemy.section09.ArrayListChallenge;


public class Contact {

    private String fContactName;
    private String fContactNumber;
    
    public Contact(String aContractName, String aContactNumber) {
        fContactName = aContractName;
        fContactNumber = aContactNumber;
    }
    
    public String getContactName(){
        return fContactName;
    }
    
    public String getContactNumber(){
        return fContactNumber;
    }
    
    public void setContactName(String aContactName){
        fContactName = aContactName;
    }
    
    public void setContactNumber(String aContactNumber){
        fContactNumber = aContactNumber;                
    }
    
    public void printContact(){
        System.out.println(fContactName+": "+fContactNumber);
    }
    
}
