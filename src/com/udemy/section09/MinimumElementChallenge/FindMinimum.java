/*
Ch - Find Minimum
*/
package com.udemy.section09.MinimumElementChallenge;

import java.util.Scanner;


public class FindMinimum {
    
    private final Scanner scanner = new Scanner(System.in);
    
    private int[] fArray = new int[0];
    
    
    public void readIntegers(int count){
        
        fArray = new int[count];
       
        System.out.println("Insert a "+count+" numbers:\r");
        for (int i=0; i<count; i++)
            fArray[i] = scanner.nextInt();
    }
    
    
    public int findMin(){
        
        int min = Integer.MAX_VALUE;
        
        for (int i=0;i<fArray.length;i++)
            if (min>fArray[i])
                min = fArray[i];
        
        return min;
    }
    
}
