package com.udemy;

import com.udemy.section09.ReverseArrayChallenge.ReverseArray;
import java.util.Arrays;

public class MainStarter {

    public static void main(String[] args) {
        System.out.println("Hello, Jacob!");


        ReverseArray ra = new ReverseArray();

        int[] array = new int[]{1,2,3,4,5};
        
        int[] reversed = ra.reverseArray(array);
        
        
        System.out.println("normal: "+Arrays.toString(array));
        System.out.println("reversed: "+Arrays.toString(reversed));

    }

}
