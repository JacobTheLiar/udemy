package com.udemy.section05;

/**
 * Write a method named getLargestPrime with one parameter of type int named
 * number. If the number is negative or does not have any prime numbers, the
 * method should return -1 to indicate an invalid value.
 *
 * The method should calculate the largest prime factor of a given number and
 * return it.
 *
 * EXAMPLE INPUT/OUTPUT: getLargestPrime (21); should return 7 since 7 is the
 * largest prime (3 * 7 = 21) getLargestPrime (217); should return 31 since 31
 * is the largest prime (7 * 31 = 217) getLargestPrime (0); should return -1
 * since 0 does not have any prime numbers getLargestPrime (45); should return 5
 * since 5 is the largest prime (3 * 3 * 5 = 45) getLargestPrime (-1); should
 * return -1 since the parameter is negative
 *
 * HINT: Since the numbers 0 and 1 are not considered prime numbers, they cannot
 * contain prime numbers. NOTE: The method getLargestPrime should be defined as
 * public static like we have been doing so far in the course. NOTE: Do not add
 * a main method to the solution code.
 * 
 * https://stackoverflow.com/questions/23287/algorithm-to-find-largest-prime-factor-of-a-number
 */
public class LargestPrime {

    public static int getLargestPrime(int aNumber) {
        if (aNumber < 2) 
            return -1;

        int divider = 2;
        while (aNumber > 1) {
            while (aNumber % divider == 0) 
                aNumber /= divider;
            
            divider++;

            if (divider * divider > aNumber && aNumber > 1) 
                return aNumber;
        }
        return divider-1;
    }
}
