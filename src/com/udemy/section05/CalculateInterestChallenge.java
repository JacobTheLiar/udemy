package com.udemy.section05;

/*
     using the for statement, call the calculateInterest method with
     the amount of 10000 with an interestRate of 2,3,4,5,6,7, and 8
     and print the results to the console window.
     How would you modify the for loop above to do the same thing as
     shown but to start from 8% and work back to 2%

     Create a for statement using any range of numbers

     Determine if the number is a prime number using the isPrime method
     if it is a prime number, print it out AND increment a count of the
     number of prime numbers found
     if that count is 3 exit the for loop
     hint:  Use the break; statement to exit
 */
public class CalculateInterestChallenge {
 
    public static void doCalculations(){
        for (int i=2; i<=8; i++){
            System.out.println("10,000 at " + i + "% interest = "+String.format("%.2f", calculateInterest(10000.0, i)));
        }
    }

    public static void doCalculationsDesc(){
        for (int i=8; i>=2; i--){
            System.out.println("10,000 at " + i + "% interest = "+String.format("%.2f", calculateInterest(10000.0, i)));
        }
    }

    public static void printPrimeNumbers(int aPrintCount){
        
        int counter = 0;
        for (int i=1; i<=10_000; i++){
            if (isPrime(i)){
                counter = counter + 1;
                System.out.println(counter+". "+i);
                if (counter == aPrintCount)
                    break;
            }
        }
        
            
            
    }
    
    
    public static double calculateInterest(double aAmount, double aInterestRate){
        return aAmount * aInterestRate / 100.0;
    }
    
    
    public static boolean isPrime(int aNumber){
        if (aNumber==1)
            return false;
        
        for (int i=2; i<=aNumber/2; i++)
            if (aNumber % i == 0)
                return false;
        
        
        return true;
        
    }
    
}
