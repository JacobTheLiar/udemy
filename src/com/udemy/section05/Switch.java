package com.udemy.section05;

/*
Create a new switch statement using char instead of int
create a new char variable
create a switch statement testing for
A, B, C, D, or E
display a message if any of these are found and then break
Add a default which displays a message saying not found
 */
public class Switch {

    
    
    public static void printCharTest(char aCharacter){
        
        
        switch (aCharacter){
            case 'A': case 'B':
                System.out.println("found 'A' or 'B'");
                break;
            case 'C':
                System.out.println("'C' as Car");
                break;
            case 'D':
                System.out.println("dede 'D'");
                break;
            case 'E':
                System.out.println("this is 'E'");
                break;
            default:
                System.out.println("nothing to do");
                break;
        }
        
    }
    
}
