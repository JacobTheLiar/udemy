package com.udemy.section05;

/*
Write a method with the name printDayOfTheWeek that has one parameter of type int and name it day.
The method should not return any value (hint: void)
Using a switch statement print gSundayh, gMondayh, c ,gSaturdayh if the int parameter gdayh is 0, 1, c , 6 respectively, otherwise it should print gInvalid dayh.
Bonus: 
	Write a second solution using if then else, instead of using switch.
	Create a new project in IntelliJ with the  name gDayOfTheWeekChallengeh
 */
public class DayOfTheWeekChallenge {
 
    public static void printDayOfWeek(int aDay){
        
        switch (aDay){
        case 1: System.out.println("Mon"); break;
        case 2: System.out.println("Tue"); break;
        case 3: System.out.println("Wed"); break;
        case 4: System.out.println("Thu"); break;
        case 5: System.out.println("Fri"); break;
        case 6: System.out.println("Sat"); break;
        case 7: System.out.println("Sun"); break;
        default: System.out.println("Invalid parameter!"); break;
    }
        
        
    }
    
}
