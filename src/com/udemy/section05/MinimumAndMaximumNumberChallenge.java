package com.udemy.section05;

import java.util.Scanner;

/**
-Read the numbers from the console entered by the user and print the minimum and maximum number the user has entered.
-Before the user enters the number, print the message gEnter number:h
-If the user enters an invalid number, break out of the loop and print the minimum and maximum number.

Hint: 
-Use an endless while loop.

Bonus: 
-Create a project with the name MinAndMaxInputChallenge.
*/
public class MinimumAndMaximumNumberChallenge {
 
    public static void executeChallenge(){
        
        Scanner scanner = new Scanner(System.in);
        
        int firstNumber = 0;
        int secondNumber = 0;
        
        boolean valid = false;
        while (!valid){
            System.out.print("Enter first number: ");
            valid = scanner.hasNextInt();
            
            if (valid){
                firstNumber = scanner.nextInt();
                System.out.println(" -> confirmed: "+firstNumber);
            } else {
                System.out.println(" fail, try agin.");
            }
            scanner.nextLine();
        }
        
        valid = false;
        while (!valid){
            System.out.print("Enter second number: ");
            valid = scanner.hasNextInt();
            
            if (valid){
                secondNumber = scanner.nextInt();
                System.out.println(" -> confirmed: "+secondNumber);
            } else {
                System.out.println(" fail, try agin.");
            }
            scanner.nextLine();
        }
        
        if (firstNumber < secondNumber)
            System.out.println(firstNumber+" < "+secondNumber);
        else if (firstNumber>secondNumber)
            System.out.println(firstNumber+" > "+secondNumber);
        else
            System.out.println(firstNumber+" = "+secondNumber);
        
        scanner.close();
        
    }
    
}
