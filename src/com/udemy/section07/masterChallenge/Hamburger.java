/*
 Ch 50 - MasterChallenge Bill Burgers
 */
package com.udemy.section07.masterChallenge;

public class Hamburger {
    
    private String burgerName;
    private String breadRollType;
    private String meat;
    private int onion;
    private int tomato;
    private int cucamber;
    private int sauce;
    
    private double baseHamburgerPrice;
    private double baseMeatPrice;
    private double baseOnionPrice;
    private double baseTomatoPrice;
    private double baseCucamberPrice;
    private double baseSaucePrice;
    
     public Hamburger(String burgerName, String breadRollType, String meat, double baseHamburgerPrice) {
        this.burgerName = burgerName;
        this.breadRollType = breadRollType;
        this.meat = meat;
        if (baseHamburgerPrice<0.01)
            this.baseHamburgerPrice = 0.01;
        else
            this.baseHamburgerPrice = baseHamburgerPrice;
        
        this.baseMeatPrice = 2.50;
        this.baseCucamberPrice = 1.20;
        this.baseOnionPrice = 0.50;
        this.baseSaucePrice = 0.70;
        this.baseTomatoPrice = 1.00;
        
        this.onion = 0;
        this.tomato = 0;
        this.cucamber = 0;
        this.sauce = 0;
    }
    
    
    public double calculatePrice(){
        double cost = baseHamburgerPrice;
        
        cost += onion*baseOnionPrice;
        cost += tomato*baseTomatoPrice;
        cost += cucamber*baseCucamberPrice;
        cost += sauce*baseSaucePrice;
        
        System.out.println("Price for "+burgerName+" with "+breadRollType+ " bread roll and "+meat+" meat");
        System.out.println("- base hamburger price: "+baseHamburgerPrice);
        System.out.println("- extra onion "+onion+" x "+baseOnionPrice+" = "+onion*baseOnionPrice);
        System.out.println("- extra tomato "+tomato+" x "+baseTomatoPrice+" = "+tomato*baseTomatoPrice);
        System.out.println("- extra cucamber "+cucamber+" x "+baseCucamberPrice+" = "+cucamber*baseCucamberPrice);
        System.out.println("- extra sauce "+sauce+" x "+baseSaucePrice+" = "+sauce*baseSaucePrice);
        return cost;
    }
    
    public void addExtraOnion(){
        onion += 1;
    }
    
    public void rmoveExtraOnion(){
        if (onion>0)
            onion -= 1;
    }
    
    public void addExtraTomato(){
        tomato += 1;
    }
    
    public void removeExtraTomato(){
        if (tomato>1)
            tomato -= 1;
    }
    
    public void addExtraCucamber(){
        cucamber += 1;
    }
    
    public void removeExtraCucamber(){
        if (cucamber>1) 
            cucamber -= 1;
    }
    
    public void addExtraSauce(){
        sauce += 1;
    }
    
    public void removeExtraSauce(){
        if (sauce>1)
            sauce -= 1;
    }
   
    
}
