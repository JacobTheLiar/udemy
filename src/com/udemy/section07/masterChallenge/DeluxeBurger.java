/*
Ch 50 - MasterChallenge Bill Burgers
*/
package com.udemy.section07.masterChallenge;

public class DeluxeBurger extends Hamburger {

    private double baseChipsPrice;
    private double baseDrinkPrice;
    
    public DeluxeBurger() {
        super("Deluxe Burger", "wheat", "beef & becon", 10.99);
        baseChipsPrice = 1.99;
        baseDrinkPrice = 2.99;
    }
    
    @Override
    public double calculatePrice(){
        double cost = super.calculatePrice();
        
        cost += baseChipsPrice; 
        cost += baseDrinkPrice;
        
        System.out.println("- extra chips 1 x "+baseChipsPrice+" = "+baseChipsPrice);
        System.out.println("- extra drink 1 x "+baseDrinkPrice+" = "+baseDrinkPrice);
        return cost;
    }        

    @Override
    public void addExtraSauce() {
        System.out.println("cant add an extra sauce in deluxe burger");
    }


    @Override
    public void addExtraCucamber() {
        System.out.println("cant add an extra cucamber in deluxe burger");
    }

    @Override
    public void addExtraTomato() {
        System.out.println("cant add an extra tomato in deluxe burger");
    }


    @Override
    public void addExtraOnion() {
        System.out.println("cant add an extra onion in deluxe burger");
    }
    
    
    
}
