/*
Ch 50 - MasterChallenge Bill Burgers
 */
package com.udemy.section07.masterChallenge;

public class HealthyBurger extends Hamburger {

    private int sprouts;
    private int lettuce;

    private double baseSproutsPrice;
    private double baseLettucePrice;
   
    
    public HealthyBurger(String meat, double baseHamburgerPrice) {
        super("HealtyBurger", "brown rye", meat, baseHamburgerPrice);
        
        sprouts = 0;
        lettuce =0;
        
        baseLettucePrice = 0.40;
        baseSproutsPrice = 0.65;
    }
    
    @Override
    public double calculatePrice(){
        double cost = super.calculatePrice();
        
        cost += sprouts*baseSproutsPrice; 
        cost += lettuce*baseLettucePrice;
        
        System.out.println("- extra sprouts "+sprouts+" x "+baseSproutsPrice+" = "+sprouts*baseSproutsPrice);
        System.out.println("- extra lettuce "+lettuce+" x "+baseLettucePrice+" = "+lettuce*baseLettucePrice);
        return cost;
    }    
    
    public void addExtraLettuce(){
        lettuce += 1;
    }
    
    public void rmoveExtraLettuce(){
        if (lettuce>0)
            lettuce -= 1;
    }

    public void addExtraSprouts(){
        sprouts += 1;
    }
    
    public void rmoveExtraSprouts(){
        if (sprouts>0)
            sprouts -= 1;
    }
    
}
