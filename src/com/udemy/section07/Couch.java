/*
Ch 45 - Composition
 */
package com.udemy.section07;

class Couch {

    private String name;
    private String style;
    private int numberOfSeats;

    public Couch(String name, String style, int numberOfSeats) {
        this.name = name;
        this.style = style;
        this.numberOfSeats = numberOfSeats;
    }

    public String getName() {
        return name;
    }

    public String getStyle() {
        return style;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }
    
    
}
