/*
Ch 45 - Composition
 */
package com.udemy.section07;

/**
Create a class and demonstate proper encapsulation techniques
the class will be called Printer
It will simulate a real Computer Printer
It should have fields for the toner Level, number of pages printed, and
also whether its a duplex printer (capable of printing on both sides of the paper).
Add methods to fill up the toner (up to a maximum of 100%), another method to
simulate printing a page (which should increase the number of pages printed).
Decide on the scope, whether to use constructors, and anything else you think is needed.
 */
public class LivingRoom {
    
    private Armchair armchair1;
    private Armchair armchair2;
    private Couch couch;
    private Lamp lamp;
    private Tv tv;

    public LivingRoom(Armchair armchair1, Armchair armchair2, Couch couch, Lamp lamp, Tv tv) {
        this.armchair1 = armchair1;
        this.armchair2 = armchair2;
        this.couch = couch;
        this.lamp = lamp;
        this.tv = tv;
    }

    public Armchair getArmchair1() {
        return armchair1;
    }

    public Armchair getArmchair2() {
        return armchair2;
    }

    public Couch getCouch() {
        return couch;
    }

    public Lamp getLamp() {
        return lamp;
    }

    public Tv getTv() {
        return tv;
    }
    
}
