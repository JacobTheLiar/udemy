/*
Ch 45 - Composition
 */
package com.udemy.section07;
class Lamp {

    private String name;
    private String bulbCount;

    public Lamp(String name, String bulbCount) {
        this.name = name;
        this.bulbCount = bulbCount;
    }

    public String getName() {
        return name;
    }

    public String getBulbCount() {
        return bulbCount;
    }
    
    
    
}
