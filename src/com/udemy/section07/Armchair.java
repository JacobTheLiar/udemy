/*
Ch 45 - Composition
 */
package com.udemy.section07;

public class Armchair {
    
    private String name;
    private String style;

    public Armchair(String name, String style) {
        this.name = name;
        this.style = style;
    }

    public String getName() {
        return name;
    }

    public String getStyle() {
        return style;
    }
    
}
