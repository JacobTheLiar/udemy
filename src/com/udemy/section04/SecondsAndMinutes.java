package com.udemy.section04;

public class SecondsAndMinutes {

    public static String getDurationString(int aMinutes, int aSeconds){
        if (!((aMinutes>=0) && (0<=aSeconds) && (aSeconds<60))){
            return "Invalid Value";
        }
        
        int hours = aMinutes / 60;
        int minutes = aMinutes % 60;
        
        return getLeadingZero(hours)  +"h "
                + getLeadingZero(minutes) + "m "
                + getLeadingZero(aSeconds) + "s";
    }
    
    
    public static String getDurationString(int aSeconds){
        return getDurationString(aSeconds/60, aSeconds%60);
    }
    
    
    public static String getLeadingZero(int aNumber){
        if (aNumber<=9)
            return "0"+aNumber;
        else
            return ""+aNumber;
    }
    
}
